import { combineReducers } from 'redux';

import RanksReducer from './reducer_ranks';

const rootReducer = combineReducers({
  ranks: RanksReducer
});

export default rootReducer;
