import React, { Component } from 'react';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';

import { getParticipants } from '../actions/index';

class RankingList extends Component {

    componentDidMount() {
        console.log('I did load');
        this.props.getParticipants().then(()=> {
            console.log('rank???: ', this.props.rank);
        });
    }
    renderList() {
        console.log('insider renderlist: ', this.props.ranks);
        const myData = [].concat(this.props.ranks)
        .sort((a, b) => a.fields.averageTime > b.fields.averageTime);
        
        return myData.map((rank, i) => {
            return (
                <tr key={i}>
                    <td>{rank.fields.name}</td><td>{rank.fields.averageTime}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <table>
                <tbody>
                    {this.renderList()}
                </tbody>
            </table>
        )
    }
}

function mapStateToProps(state) {
    return {
        ranks: state.ranks
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({getParticipants : getParticipants}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RankingList);