import React, { Component } from 'react';
import './App.css';

import RankingList from './containers/ranking_list';
import ApplicationForm from './components/application_form';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {page: 'RankingList'};
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Mario Kart for Kids</h1>
        </header>
        <button onClick={() => this.setState({page: 'RankingList'})}>Rankings</button>
        <button onClick={() => this.setState({page: 'ApplicationForm'})}>Sign Up</button>
        {/* <RankingList /> */}
        {this.whichComponent(this.state.page)}
      </div>
    );
  }

  whichComponent(page) {
    if (page === 'RankingList') {
      return (<RankingList />)
    } else if (page === 'ApplicationForm') {
      return (<ApplicationForm />)
    }
  }
}

export default App;
